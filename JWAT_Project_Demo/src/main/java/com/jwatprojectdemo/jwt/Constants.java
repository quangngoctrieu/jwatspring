package com.jwatprojectdemo.jwt;

public class Constants {
	public static final String API_SECRET_KEY = "JWAT";

	public static final long TOKEN_VALIDITY = 2*60*60*1000;

}
