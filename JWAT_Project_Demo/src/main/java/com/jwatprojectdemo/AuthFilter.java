package com.jwatprojectdemo;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpStatus;
import org.springframework.web.filter.GenericFilterBean;

import com.jwatprojectdemo.jwt.Constants;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;

// decode token
public class AuthFilter extends GenericFilterBean {

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		// TODO Auto-generated method stub
		HttpServletRequest httpServletRequest = (HttpServletRequest) request;
		HttpServletResponse httpServletResponse =  (HttpServletResponse) response;
		
		String authHeader = httpServletRequest.getHeader("Authorization");
	
		if(authHeader != null) {
			String token = authHeader;
			try {
				Claims claims = Jwts.parser().setSigningKey(Constants.API_SECRET_KEY)
						.parseClaimsJws(token).getBody();
				httpServletRequest.setAttribute("username", claims.get("username").toString());
				
			} catch (Exception e) {
				// TODO: handle exception
				httpServletResponse.sendError(HttpStatus.FORBIDDEN.value(),"invalid/expired token");
				return;
			}
			
		}else {
			httpServletResponse.sendError(HttpStatus.FORBIDDEN.value(),"Authorization must have token");
			return;
			
		}
		chain.doFilter(request, response);
		
	}

}
