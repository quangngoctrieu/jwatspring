package com.jwatprojectdemo.controller;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.jwatprojectdemo.bean.Account;
import com.jwatprojectdemo.jwt.Constants;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@Controller
@RequestMapping("/admin")
public class HomeController {
	@GetMapping("")
	public String index() {
		return "admin/index";
	}
	
	//encode token
		private Map<String, String> generateJWTToken(Account account) {
			long timestamp = System.currentTimeMillis();
			String token = Jwts.builder().signWith(SignatureAlgorithm.HS256, Constants.API_SECRET_KEY)
					.setIssuedAt(new Date(timestamp))
					.setExpiration(new Date(timestamp + Constants.TOKEN_VALIDITY))
					.claim("username", account.getUsername())
					.claim("fullname", account.getFullname())
					.claim("email", account.getEmail())
					.claim("photo", account.getPhoto())
					.compact();
			Map<String , String> map = new HashMap<String, String>();
			map.put("token", token);

			return map;

		}
}
