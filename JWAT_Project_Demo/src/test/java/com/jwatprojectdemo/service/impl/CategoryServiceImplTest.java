package com.jwatprojectdemo.service.impl;

import static org.junit.jupiter.api.Assertions.*;

import com.jwatprojectdemo.bean.Category;
import com.jwatprojectdemo.bean.Category;
import com.jwatprojectdemo.repository.CategoryRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.Rollback;

import java.util.List;

@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@Rollback(true)
public class CategoryServiceImplTest {

	@Autowired
	CategoryRepository ca;
	@Test
	public void testFindAll() {
		List<Category> Category = ca.findAll();
		assertEquals(4,Category.size());
	}

	@Test
	void testFindById() {
		String id="DT";
		Category Category = ca.findById(id).get();
		assertEquals(id,Category.getId());
	}

	@Test
	public void testFindByKeyword() {
		String id="apple";
		Category category = (Category) ca.findByName("%"+id+"%");
		assertEquals(id,category.getId());
	}

	@Test
	void testSave() {

		Category category = new Category();
		category.setId("QNT");
		category.setName("Trieu");

		Category categorySave= ca.save(category);
		category = ca.findById("QNT").get();
		assertEquals("QNT",category.getId());
	}

	@Test
	public void testDeleteById() {
		String id="DT";

		ca.deleteById(id);
		Boolean Category = ca.findById(id).isPresent();
		assertEquals(false,Category);
	}

	@Test
	void testExistById() {
		String id="DT";
		Boolean Category = ca.existsById(id);
		assertEquals(true,Category);
	}

}
