package com.jwatprojectdemo.service.impl;

import static org.junit.jupiter.api.Assertions.*;

import com.jwatprojectdemo.bean.Account;
import com.jwatprojectdemo.repository.AccountRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.Rollback;

import java.util.List;

@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@Rollback(true)
class AccountServiceImplTest {
	@Autowired
	AccountRepository accountRepository;
	@Test
	public void testFindAll() {
		List<Account> account = accountRepository.findAll();
		assertEquals(1,account.size());
	}

	@Test
	void testFindById() {
//		String id="apple";
//		Account account = accountRepository.findById(id).get();
//		assertEquals(id,account.getUsername());
	}

	@Test
	void testFindByKeyword() {

	}

	@Test
	void testSave() {

		Account account = new Account();
		account.setUsername("trieu");
		account.setEmail("quangtrieu@gmail.com");
		account.setPassword("123456");
		account.setFullname("Quang Ngoc Trieu");
		account.setPhoto("abc.png");
		account.setActivated(true);
		Account accountSave= accountRepository.save(account);
		List<Account> accoun = accountRepository.findAll();
		assertEquals(2,accoun.size());
	}

	@Test
	public void testDeleteById() {
		String id="trieu";

		accountRepository.deleteById(id);
		Boolean account = accountRepository.findById(id).isPresent();
		assertEquals(false,account);
	}

	@Test
	void testExistById() {
		String id="phucnh";
		Boolean account = accountRepository.existsById(id);
		assertEquals(true,account);
	}

}
