package com.jwatprojectdemo.service.impl;

import static org.junit.jupiter.api.Assertions.*;


import com.jwatprojectdemo.bean.Brand;

import com.jwatprojectdemo.repository.BrandRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.Rollback;

import java.util.List;


@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@Rollback(true)
public class BrandServiceImplTest {

	@Autowired
	BrandRepository brandRepository;
	@Test
	public void testFindAll() {
		List<Brand> brand = brandRepository.findAll();
		assertEquals(8,brand.size());
	}

	@Test
	void testFindById() {
		String id="apple";
		Brand brand = brandRepository.findById(id).get();
		assertEquals(id,brand.getId());
	}

	@Test
	void testFindByKeyword() {
		String id="apple";
		Brand brand = (Brand) brandRepository.findByName("%"+id+"%");
		assertEquals(id,brand.getId());
	}

	@Test
	void testSave() {
		String id="redmi";
		String name="Redmi";
		String image="b70171f9.png";
		Brand brand = new Brand();
		brand.setId(id);
		brand.setName(name);
		brand.setImage(image);
		Brand brandSave=brandRepository.save(brand);
		brand = brandRepository.findById(id).get();
		assertEquals(id,brand.getId());
	}

	@Test
	public void testDeleteById() {
		String id="oppo";

		brandRepository.deleteById(id);
		Boolean brand = brandRepository.findById(id).isPresent();
		assertEquals(false,brand);
	}

	@Test
	void testExistById() {
		String id="apple";
		Boolean brand = brandRepository.existsById(id);
		assertEquals(true,brand);
	}

}
