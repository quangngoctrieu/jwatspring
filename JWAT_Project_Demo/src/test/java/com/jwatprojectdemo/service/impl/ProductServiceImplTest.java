package com.jwatprojectdemo.service.impl;

import static org.junit.jupiter.api.Assertions.*;

import com.jwatprojectdemo.bean.Category;
import com.jwatprojectdemo.bean.Product;
import com.jwatprojectdemo.repository.ProductRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.Rollback;

import java.util.List;

@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@Rollback(true)
class ProductServiceImplTest {

	@Autowired
	ProductRepository pro;
	@Test
	public void testFindAll() {
		List<Product> pro = this.pro.findAll();
		assertEquals(6,pro.size());
	}

	@Test
	void testFindById() {
		Long id= Long.valueOf(5);
		Product product = pro.findById(id).get();
		assertEquals(id,product.getId());
	}

	@Test
	public void testFindByKeyword() {
		String id="OPPO";
		Product product = (Product) pro.findByKeyword("%"+id+"%");
		assertEquals(id,product.getId());
	}

	@Test
	void testSave() {
		Long id= Long.valueOf(11);
		Product product = new Product();
		product.setId(id);
		product.setName("Trieu");

		Product categorySave= pro.save(product);
		product = pro.findById(id).get();
		assertEquals(id,product.getId());
	}

	@Test
	public void testDeleteById() {
		Long id= Long.valueOf(5);

		pro.deleteById(id);
		Boolean product = pro.findById(id).isPresent();
		assertEquals(false,product);
	}

	@Test
	void testExistById() {
		Long id= Long.valueOf(5);
		Boolean product = pro.existsById(id);
		assertEquals(true,product);
	}

}
